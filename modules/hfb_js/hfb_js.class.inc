<?php

/**
 * Supply support for the Facebook Javascript SDK to Harness facebook.
 *
 * To instantiate this class either perform:
 *   $hfb = HarnessFacebook::singleton();
 *   $hfb_js = $hfb->HarnessFacebookJs();
 * Or:
 *   $hfb_js = HarnessFacebookJs::singleton();
 */
class HarnessFacebookJs implements HarnessFacebookInterface {
  /**
   * Singleton instance.
   *
   * @var HarnessFacebook
   */
  private static $instance;

  /**
   * Storage for overloaded data.
   *
   * With the exception of the asynchronous and locale keys all of these default
   * values come from the Facebook FB.init documentation.
   *
   * @var array
   *
   * @see https://developers.facebook.com/docs/reference/javascript/FB.init/
   */
  protected $data = array(
    'authResponse' => TRUE, // Object also retrievable by getAuthResponse. Replaces session when oauth:true.
    'appId' => NULL, // Your application ID
    'asynchronous' => TRUE, // Whether to use asynchronous loading, recommended.
    'channelUrl' => NULL, // Specifies the URL of a custom URL channel file. This file must contain a single script element pointing to the JavaScript SDK URL.
    'cookie' => FALSE, // true to enable cookie support.
    'locale' => 'en_US', // Default locale to utilize.
    'logging' => TRUE, // false to disable logging.
    'oauth' => FALSE, // true to enable OAuth 2.0.
    'status' => TRUE, // true to fetch fresh status.
    'xfbml' => FALSE, // true to parse XFBML tags.
  );

  /**
   * Storage for attached JavaScript.
   *
   * @var array
   */
  protected $js = array();

  private function __construct() {
    // It is strongly recommend that the channel is always used. This module
    // automatically provides a channel.html menu hook and provides all the hook
    // up, only override this variable to false in your settings.php if you have
    // a specific reason.
    if (variable_get('hfb_js_var_auto_attach_channel', TRUE)) {
      // If you'd like to use your own channel URL you may provide it in your
      // settings.php with this variable, however I see no reason to do so with
      // it's limited purpose.
      if ($channel_url = variable_get('hfb_js_var_channel_override', FALSE)) {
        $this->channelUrl = $channel_url;
      }
      // Else attach our default channel.html.
      else {
        global $base_url;
        $this->channelUrl = substr($base_url, strpos($base_url, ':') + 1) . '/channel.html';
      }
    }
  }

  /**
   * Singleton static function for accessing/building the instance.
   *
   * @return HarnessFacebookJs
   */
  public static function singleton() {
    if (!isset(self::$instance)) {
      $className = __CLASS__;
      self::$instance = new $className;
    }
    return self::$instance;
  }

  /**
   * Detect whether the class has been instantiated.
   *
   * Used to determine whether the JS SDK should be output.
   *
   * @return boolean
   */
  public static function instantiated() {
    return isset(self::$instance);
  }

  /**
   * Prevent cloning.
   */
  final private function __clone() {}

  /**
   * Isset magic method.
   *
   * @param string $name
   *
   * @return boolean
   */
  public function __isset($name) {
    return isset($this->data[$name]);
  }

  /**
   * Get magic method.
   *
   * @param string $name
   *
   * @return mixed
   */
  public function __get($name) {
    if (array_key_exists($name, $this->data)) {
      return $this->data[$name];
    }
  }

  /**
   * Set magic method.
   *
   * @param string $name
   * @param mixed $value
   *
   * @return void
   */
  public function __set($name, $value) {
    switch ($name) {
      // Boolean properties.
      case 'authResponse':
      case 'asynchronous':
      case 'cookie':
      case 'logging':
      case 'oauth':
      case 'status':
      case 'xfbml':
        $this->data[$name] = (bool) $value;
        break;
      // Any other properties.
      default:
        $this->data[$name] = $value;
    }
  }

  /**
   * Convert to string magic method.
   *
   * Used to generate the facebook SDK inclusion code.
   *
   * @return string
   */
  public function __toString() {
    $string = '<div id="fb-root"></div>';

    // Prepare initialization.
    $fb_init = 'FB.init({'
      . 'authResponse:' . var_export($this->authResponse, TRUE) . ','
      . (isset($this->appId) ? 'appId:\'' . $this->appId . '\',' : '')
      . (isset($this->channelUrl) ? 'channelUrl:\'' . $this->channelUrl . '\',' : '')
      . 'logging:' . var_export($this->logging, TRUE) . ','
      . 'status:' . var_export($this->status, TRUE) . ','
      . 'cookie:' . var_export($this->cookie, TRUE) . ','
      . 'oauth:' . var_export($this->oauth, TRUE) . ','
      . 'xfbml:' . var_export($this->xfbml, TRUE)
      . '});';

    // Load additional initilization code in $fb_init

    // Attach the fb_init code, note that the <script> tag is left open.
    if ($this->asynchronous) {
      $string .= '<script>window.fbAsyncInit=function(){' . $fb_init . '};';
    }
    else {
      $string .= '<script src="//connect.facebook.net/en_US/all.js"></script>';
      $string .= '<script>' . $fb_init;
    }

    // Load the SDK
    $string .= '(function(d){'
        . 'var js, id = \'facebook-jssdk\'; if (d.getElementById(id)) {return;}'
        . 'js = d.createElement(\'script\'); js.id = id; js.async = true;'
        . 'js.src = "//connect.facebook.net/' . $this->locale . '/all.js";'
        . 'd.getElementsByTagName(\'head\')[0].appendChild(js);'
        . '}(document));'
        // Close the <script> tag left open from the fb_init attachment.
        . '</script>';

    return $string;
  }

  /**
   * Unset magic method.
   *
   * @return void
   */
  public function __unset($name) {
    unset($this->data[$name]);
  }

  /**
   * Attach JavaScript to the facebook initialization code.
   *
   * @param string $uid
   *   A unique ID to apply, this is should be some identifier unique to your
   *   module. This is used as a drupal_alter() $type upon attachment.
   *   Example: my_module_name__ui will call hook_my_module_name__ui_alter(&$js)
   * @param string $js
   *   The JavaScript to attach to the Facebook initialization code. This is
   *   direct JavaScript, do not wrap with <script> tags.
   *
   * @return HarnessFacebookJs
   */
  public function attachJs($uid, $js) {
    drupal_alter($uid, $js);

    $this->js[$uid] = $js;

    return $this;
  }
}
