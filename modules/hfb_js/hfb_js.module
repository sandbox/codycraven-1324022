<?php

/**
 * @file
 * Hook implementations for the Harness facebook JavaScript SDK module.
 */

/**
 * Implements hook_menu().
 */
function hfb_js_menu() {
  $items['channel.html'] = array(
    'access callback' => TRUE,
    'page callback' => 'hfb_js_menu_channel',
    'title' => 'Facebook channel',
  );

  return $items;
}

/**
 * Menu callback for Facebook channel file.
 *
 * Addresses some issues with cross domain communication in certain browsers.
 */
function hfb_js_menu_channel() {
  // Cache channel file for as long as possible for the user. Without proper
  // caching, cross domain communication will become very slow and users will
  // suffer a severely degraded experience.
  $cache_expire = 60 * 60 * 24 * 365;
  header('Pragma: public');
  header('Cache-Control: max-age=' . $cache_expire);
  header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $cache_expire) . ' GMT');

  $hfb_js = HarnessFacebookJs::singleton();
  print '<script src="//connect.facebook.net/' . $hfb_js->locale . '/all.js"></script>';
  exit();
}

/**
 * Implements hook_page_alter().
 */
function hfb_js_page_alter(&$page) {
  // If the JS SDK was not invoked by any code then don't output the SDK.
  if (!HarnessFacebookJs::instantiated()) {
    return;
  }

  $hfb_js = HarnessFacebookJs::singleton();
  $sdk_build = array(
    '#markup' => (string) $hfb_js,
    '#type' => 'markup',
  );

  // If asynchronous load at top of page.
  if ($hfb_js->asynchronous) {
    $page['page_top']['harness_facebook_js_sdk'] = $sdk_build;
    $page['page_top']['harness_facebook_js_sdk']['#weight'] = variable_get('hfb_js_var_page_top_weight', -500);
  }
  // Otherwise at bottom.
  else {
    $page['page_bottom']['harness_facebook_js_sdk'] = $sdk_build;
    $page['page_bottom']['harness_facebook_js_sdk']['#weight'] = variable_get('hfb_js_var_page_bottom_weight', 0);
  }
}

/**
 * Implements hook_rdf_namespaces().
 */
function hfb_js_rdf_namespaces() {
  // If the JS SDK was not invoked by any code then don't output the SDK.
  if (!HarnessFacebookJs::instantiated()) {
    return;
  }

  // Add an XML namespace to the html tag if XFBML is being supported.
  $hfb_js = HarnessFacebookJs::singleton();
  
  if ($hfb_js->xfbml) {
    return array('fb' => 'http://ogp.me/ns/fb#');
  }
}
