<?php

/**
 * Supply support for the Facebook PHP SDK to Harness facebook.
 *
 * To instantiate this class either perform:
 *   $hfb = HarnessFacebook::singleton();
 *   $hfb_php = $hfb->HarnessFacebookPhp();
 * Or:
 *   $hfb_php = HarnessFacebookPhp::singleton();
 */
class HarnessFacebookPhp implements HarnessFacebookInterface {
  /**
   * Singleton instance.
   *
   * @var HarnessFacebook
   */
  private static $instance;

  private function __construct() {}

  /**
   * Singleton static function for accessing/building the instance.
   *
   * @return HarnessFacebookPhp
   */
  public static function singleton() {
    if (!isset(self::$instance)) {
        $className = __CLASS__;
        self::$instance = new $className;
    }
    return self::$instance;
  }

  /**
   * Prevent cloning.
   */
  final private function __clone() {}

}
