<?php

/**
 * Singleton base class for facebook API interactions.
 */
class HarnessFacebook implements HarnessFacebookInterface {
  /**
   * Singleton instance.
   *
   * @var HarnessFacebook
   */
  private static $instance;

  /**
   * Array of singleton style class instances used by the __call() magic method.
   *
   * @var array
   */
  protected $apiExtensions = array();

  /**
   * Access the instance by calling HarnessFacebook::singleton().
   */
  private function __construct() {}

  /**
   * Singleton static function for accessing/building the instance.
   *
   * @return HarnessFacebook
   */
  public static function singleton() {
    if (!isset(self::$instance)) {
        $className = __CLASS__;
        self::$instance = new $className;
    }
    return self::$instance;
  }

  /**
   * Call magic method, returns class instance of the method name.
   *
   * @param string $name
   *   The name of the method called, this acts as an autoloader for classes,
   *   supporting a singleton structure.
   * @param array $args
   *   Args are discarded in HarnessFacebook as it is a simple loader for other
   *   Facebook APIs.
   */
  public function __call($name, $args) {
    // If API Extension is not yet registered then register it.
    if (!isset($this->apiExtensions[$name])) {
      if (class_exists($name, TRUE)) {
        // Attach new API extension
        // We really could check this through reflection of the interface but it
        // doesn't seem needed.
        if (method_exists($name, 'singleton')) {
          $this->apiExtensions[$name] = call_user_method('singleton', $name);
        }
        else {
          throw HarnessFacebookException(t('The class %class is not defined as a singleton so it can not be loaded as a HarnessFacebook API extension.', array('%class' => $name)));
        }
      }
      else {
        throw HarnessFacebookException(t('The class %class is unloadable.', array('%class' => $name)));
      }
    }

    return $this->apiExtensions[$name];
  }

  /**
   * Prevent cloning.
   */
  final private function __clone() {}
}

/**
 * Interface for our singleton pattern.
 */
interface HarnessFacebookInterface {
  /**
   * For any class that directly operates on HarnessFacebook we want singletons.
   */
  public static function singleton();
}

/**
 * Extended exception class for use by HarnessFacebook class.
 */
class HarnessFacebookException extends Exception {}
